<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * Class QuestionAdmin
 *
 * @package AppBundle\Admin
 * @author  Calin Pristavu <calin.pristavu@evozon.com>
 */
class QuestionAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('text', 'text')
            ->add('multiple', 'sonata_type_boolean')
            ->add('answers', 'sonata_type_collection', [
                'by_reference' => false
            ], [
                'edit' => 'inline',
                'inline' => 'table',
            ]);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('text')
            ->addIdentifier('multiple')
            ->addIdentifier('answers')
            ->add('_action', 'actions', [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => []
                ]
            ]);
    }
}
