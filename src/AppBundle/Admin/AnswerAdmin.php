<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * Class AnswerAdmin
 *
 * @package AppBundle\Admin
 * @author  Calin Pristavu <calin.pristavu@evozon.com>
 */
class AnswerAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('text', 'text')
            ->add('correct', 'sonata_type_boolean');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('text')
            ->addIdentifier('correct');
    }
}
