<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;

/**
 * Class QuizAdmin
 *
 * @package AppBundle\Admin
 * @author  Calin Pristavu <calin.pristavu@evozon.com>
 */
class QuizAdmin extends Admin
{
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('questions')
            ->add('_action', 'actions', [
                'actions' => [
                    'show' => []
                ]
            ]);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
    }

}
