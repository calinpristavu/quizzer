<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Question
 *
 * @package AppBundle\Entity
 * @author  Calin Pristavu <calin.pristavu@evozon.com>
 * @ORM\Entity(repositoryClass="AppBundle\Repository\QuestionRepository")
 * @ORM\Table(name="question")
 */
class Question
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    protected $text;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Answer", mappedBy="question", cascade={"persist", "remove"})
     */
    protected $answers;

    /**
     * @var bool
     *
     * @ORM\Column(name="multiple", type="boolean")
     */
    protected $multiple;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Quiz", mappedBy="questions")
     */
    protected $quizzes;

    /**
     * Question constructor.
     */
    public function __construct()
    {
        $this->text = '';
        $this->answers = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     *
     * @return Question
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @param $text
     *
     * @return Question
     */
    public function appendText($text)
    {
        $this->text = $this->text . $text;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    public function getCorrectAnswers()
    {
        return $this->answers->filter(function(Answer $answer){
            return $answer->isCorrect();
        });
    }

    /**
     * @param Answer $answer
     *
     * @return Question
     */
    public function addAnswer(Answer $answer)
    {
        $this->answers->add($answer);
        $answer->setQuestion($this);

        return $this;
    }

    /**
     * @param Answer $answer
     *
     * @return Question
     */
    public function removeAnswer($answer)
    {
        $this->answers->removeElement($answer);

        return $this;
    }

    /**
     * @return boolean
     */
    public function isMultiple()
    {
        return $this->multiple;
    }

    /**
     * @param boolean $multiple
     *
     * @return Question
     */
    public function setMultiple($multiple)
    {
        $this->multiple = $multiple;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuizzes()
    {
        return $this->quizzes;
    }

    /**
     * @param mixed $quizzes
     *
     * @return Question
     */
    public function setQuizzes($quizzes)
    {
        $this->quizzes = $quizzes;

        return $this;
    }
}
