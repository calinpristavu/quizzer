<?php
declare(strict_types = 1);

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Quiz
 *
 * @package AppBundle\Entity
 * @author  Calin Pristavu <calin.pristavu@evozon.com>
 * @ORM\Entity(repositoryClass="AppBundle\Repository\QuizRepository")
 * @ORM\Table(name="quiz")
 */
class Quiz
{
    const DEFAULT_NO_QUESTIONS = 15;

    const PASS_PERCENT = 65;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Question", inversedBy="quizzes")
     * @ORM\JoinTable(name="quizz_questions")
     */
    protected $questions;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Answer")
     * @ORM\JoinTable(name="quizz_answers",
     *      joinColumns={@ORM\JoinColumn(name="quiz_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="answer_id", referencedColumnName="id")}
     * )
     */
    protected $answers;

    /**
     * @ORM\Column(type="integer", options={"default": 0})
     */
    protected $score;

    /**
     * @ORM\Column(type="integer", options={"default": 0})
     */
    protected $noOfQuestions;

    /**
     * @ORM\Column(type="integer", options={"default": 0})
     */
    protected $noCorrectAnswers;

    /**
     * @var Question
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Question")
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id")
     */
    protected $activeQuestion;

    /**
     * @ORM\Column(type="integer", options={"default": 0})
     */
    protected $answeredQuestions;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="quizzes")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * Quiz constructor.
     */
    public function __construct()
    {
        $this->questions = new ArrayCollection();
        $this->answers = new ArrayCollection();
        $this->score = 0;
        $this->noOfQuestions = self::DEFAULT_NO_QUESTIONS;
        $this->noCorrectAnswers = 0;
        $this->answeredQuestions = 0;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return ArrayCollection
     */
    public function getQuestions(): ArrayCollection
    {
        return $this->questions;
    }

    /**
     * @param Question $question
     *
     * @return Quiz
     */
    public function addQuestion($question): Quiz
    {
        $this->questions->add($question);

        return $this;
    }

    /**
     * @param Question $question
     *
     * @return Quiz
     */
    public function removeQuestion($question): Quiz
    {
        $this->questions->remove($question);

        return $this;
    }

    /**
     * @return int
     */
    public function getScore(): int
    {
        return (string) $this->score;
    }

    /**
     * @param int $score
     *
     * @return Quiz
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNoOfQuestions()
    {
        return $this->noOfQuestions;
    }

    /**
     * @param mixed $noOfQuestions
     *
     * @return Quiz
     */
    public function setNoOfQuestions($noOfQuestions)
    {
        $this->noOfQuestions = $noOfQuestions;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * @param ArrayCollection $answers
     *
     * @return ArrayCollection
     */
    public function setAnswers(ArrayCollection $answers)
    {
        return $this->answers = $answers;
    }

    /**
     * @param Answer $answer
     *
     * @return Quiz
     */
    public function addAnswer(Answer $answer)
    {
        $this->answers->add($answer);

        return $this;
    }

    /**
     * @param Answer $answer
     *
     * @return Quiz
     */
    public function removeAnswer(Answer $answer)
    {
        $this->answers->removeElement($answer);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNoCorrectAnswers()
    {
        return $this->noCorrectAnswers;
    }

    /**
     * @param mixed $noCorrectAnswers
     *
     * @return Quiz
     */
    public function setNoCorrectAnswers($noCorrectAnswers)
    {
        $this->noCorrectAnswers = $noCorrectAnswers;

        return $this;
    }

    /**
     * @return Quiz
     */
    public function incrementCorrect()
    {
        $this->noCorrectAnswers++;

        return $this;
    }

    /**
     * @return Question
     */
    public function getActiveQuestion()
    {
        return $this->activeQuestion;
    }

    /**
     * @param Question $activeQuestion
     *
     * @return Quiz
     */
    public function setActiveQuestion($activeQuestion)
    {
        $this->activeQuestion = $activeQuestion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAnsweredQuestions()
    {
        return $this->answeredQuestions;
    }

    /**
     * @param mixed $answeredQuestions
     *
     * @return Quiz
     */
    public function setAnsweredQuestions($answeredQuestions)
    {
        $this->answeredQuestions = $answeredQuestions;

        return $this;
    }

    /**
     * @return Quiz
     */
    public function incrementAnswered()
    {
        $this->answeredQuestions++;

        return $this;
    }

    /**
     * @param User $user
     *
     * @return Quiz
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

}
