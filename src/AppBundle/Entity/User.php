<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * User
 *
 * @author  Calin Bolea <calin.bolea@evozon.com>
 * @ORM\Table(name="user")
 * @ORM\Entity
 */
class User extends BaseUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Quiz", mappedBy="user", cascade={"persist", "remove"})
     */
    protected $quizzes;

    public function __construct()
    {
        parent::__construct();
        $this->quizzes = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return ArrayCollection
     */
    public function getQuizzes()
    {
        return $this->quizzes;
    }

    /**
     * @param Quiz $quiz
     *
     * @return Quiz
     */
    public function addQuiz($quiz)
    {
        $this->quizzes->add($quiz);
        $quiz->setUser($this);

        return $this;
    }
}

