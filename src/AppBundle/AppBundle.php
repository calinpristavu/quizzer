<?php

namespace AppBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class AppBundle extends Bundle
{
    /**
     * Boots the Bundle.
     */
    public function boot()
    {
        parent::boot(); // TODO: Change the autogenerated stub
    }

    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
