<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Question;
use AppBundle\Entity\Answer;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class Fixture1
 *
 * @package AppBundle\DataFixtures\ORM
 * @author  Calin Pristavu <calin.pristavu@evozon.com>
 */
class Fixture1 implements FixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $em
     */
    public function load(ObjectManager $em)
    {
        $question = new Question();
        $question->setText("Where does a compiler pass(CompilerPass) of the service container must be registered to be executed?");
        $question->setMultiple(false);
        $answer = new Answer();
        $answer->setText("It has to be registered on the ContainerBuilder object received as an argument of the load() method in a bundle extension class.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("It must be registered as a service. The latter must be tagged with the special \"compiler_pass\" tag.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("The compiler passes are automatically registered if they're hosted under the DependencyInjection/Compiler directory of any bundles.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("It has to be registered in the registerContainerConfiguration method of the AppKernel class.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $em->persist($question);

        $question = new Question();
        $question->setText("Which one of the following concepts of the OOP allows a class to define several methods with the same name but with different parameters?");
        $question->setMultiple(false);
        $answer = new Answer();
        $answer->setText("Encapsulation");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Abstraction");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Inheritance");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Polymorphism");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Visibility");
        $question->addAnswer($answer);
        $em->persist($answer);
        $em->persist($question);

        $question = new Question();
        $question->setText("Which of these assertions are true for abstract classes?");
        $question->setMultiple(true);
        $answer = new Answer();
        $answer->setText("An abstract class has to implement at least an interface.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("An abstract class has to be specialized(inherited/extended)");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("An abstract class contains only abstract methods.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("An abstract class doesn't contain attributes.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("An abstract class can't be instantiated");
        $question->addAnswer($answer);
        $em->persist($answer);
        $em->persist($question);

        $question = new Question();
        $question->setText("Which of the following assertions is valid to define an algorithm?");
        $question->setMultiple(false);
        $answer = new Answer();
        $answer->setText("An algorithm is a program written in a particular programming language.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("An algorithm is a finished and non-ambiguous series of statements allowing to answer a particular problem. ");
        $question->addAnswer($answer);
        $em->persist($answer);
        $em->persist($question);

        $question = new Question();
        $question->setText("Which of these assertions are true about the unique tokens stored in an hidden field of a Symfony form?");
        $question->setMultiple(true);
        $answer = new Answer();
        $answer->setText("It is deativable.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("It checks if the form is not submitted by a robot.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("It increases performance of the framework.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("It requires that the session is active.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("It maintains the user's session active.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $em->persist($question);

        $question = new Question();
        $question->setText('In the following code snippet, what\'s the meaning of the two constructor arguments of the AppKernel class?
<pre>
use Symfony\Component\ClassLoader\ApcClassLoader;
use Symfony\Component\HttpFoundation\Request;

$loader = require_once __DIR__.\'/../app/bootstrap.php.cache\';

$kernel = new AppKernel(\'app\', true);
$kernel->loadClassCache();

$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
</pre>');
        $question->setMultiple(true);
        $answer = new Answer();
        $answer->setText("The environment name.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("The cache activation.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("The application name.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("The \"debug\" mode.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("The activation of automatic updates.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $em->persist($question);

        $question = new Question();
        $question->setText("Can PHP support multiple inheritance?");
        $question->setMultiple(false);
        $answer = new Answer();
        $answer->setText("Yes, but only for interfaces");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("No");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Yes, but only for classes.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Yes, both for classes and interfaces.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $em->persist($question);

        $question = new Question();
        $question->setText('Why is the following class not valid?
<pre>
class Animal
{
	private $species;

	abstract function isPredator();

	public function setSpecies($species)
	{
		$this->species = $species;
	}
}
</pre>');
        $question->setMultiple(false);
        $answer = new Answer();
        $answer->setText("It implements no interfaces.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("The isPredator() method does not have any visibility.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("It must be declared abstract.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("The setSpecies() concrete method can't be defined as the class has at least one abstract method.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("There is no constructor.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $em->persist($question);

        $question = new Question();
        $question->setText("What makes the Symfony 2.3 LTS version so special?");
        $question->setMultiple(false);
        $answer = new Answer();
        $answer->setText("It is distributed under a business license than the other versions of Symfony.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("It is 150% times more efficient than the other versions of Symfony.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("It is fully backward compatible with 2.0, 2.1, 2.2 versions.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("It is developed by a different team than the \"core team\"");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("It is supported during 4 years: 3 years for the bug fixes and one extra year for the security fixes.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $em->persist($question);

        $question = new Question();
        $question->setText("10. Which of the following define a variable in algorithmics?");
        $question->setMultiple(true);
        $answer = new Answer();
        $answer->setText("A value");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("A name");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("A type");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("A scope");
        $question->addAnswer($answer);
        $em->persist($answer);
        $em->persist($question);

        $question = new Question();
        $question->setText("In the following app/config/security.yml file source code:
<pre>
security:
	WWW:
		Symfony\Component\Security\Core\User\User: plaintext
	XXX:
		admin:
			entity: { class: AcmeDemoBundle:User }
	YYY:
		admin:
			pattern: ^/admin
			http_basic: ~
	ZZZ:
		- { path: ^/admin, roles: ROLE_ADMIN }
</pre>
Which words do WWW, XXX, YYY and ZZZ markers replace?");
        $question->setMultiple(true);
        $answer = new Answer();
        $answer->setText("access_controll");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("firewalls");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("role_hierarchy");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("providers");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("encoders");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("access_decision_manager");
        $question->addAnswer($answer);
        $em->persist($answer);
        $em->persist($question);

        $question = new Question();
        $question->setText("Which of the following bundles are considered Symfony \"core bundles\" and maintained by the Symfony core team?");
        $question->setMultiple(true);
        $answer = new Answer();
        $answer->setText("WebProfilerBundle");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("DoctrineBundle");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("FrameworkBundle");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("TwigBundle");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("AsseticBundle");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("SecurityBundle");
        $question->addAnswer($answer);
        $em->persist($answer);
        $em->persist($question);

        $question = new Question();
        $question->setText("Is the following code valid?
<pre>
namespace Application\Exception;
class Exception extends \Exception
{
}
</pre>");
        $question->setMultiple(false);
        $answer = new Answer();
        $answer->setText("No");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Yes");
        $question->addAnswer($answer);
        $em->persist($answer);
        $em->persist($question);

        $question = new Question();
        $question->setText("Which role has an index on a specific field of a table?");
        $question->setMultiple(false);
        $answer = new Answer();
        $answer->setText("The index simplifies and speeds up the lookup operations, sorting, joins or aggregations.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("The index ensures that the record has been stored in the database.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("The index forbids the deletion of a record from the table if it is not related to another record.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $em->persist($question);

        $question = new Question();
        $question->setText("In our solar system, the distance between the Earth and the Sun is about 150 millions of kilometers. Which data type is the most appropriate to store this distance in meters in a field called distance in a MySQL database?");
        $question->setMultiple(false);
        $answer = new Answer();
        $answer->setText("INTEGER UNSIGNED");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("MEDIUMINT UNSIGNED");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("MEDIUMINT");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("BIGINT");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("BIGINT UNSIGNED");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("SMALLINT");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("INTEGER");
        $question->addAnswer($answer);
        $em->persist($answer);
        $em->persist($question);

        $question = new Question();
        $question->setText('In the following code sample, which of these assertions are true about the flush() method of the entity manager?
<pre>
$entity = new OneEntity();
$entity->setFoo(\'bar\');

$em = $this->getDoctrine()->getManager();
$em->persist($entity);
$em->flush();
</pre>');
        $question->setMultiple(true);
        $answer = new Answer();
        $answer->setText("It executes all the SQL queries inside a transaction and cancels in case of failure.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("It empties all the tables of the database.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("It tries to optimize the number of SQL queries.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("It empties the Doctrine entities' cache.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("It executes each SQL queries in a separate transaction.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $em->persist($question);

        $question = new Question();
        $question->setText("Which of these mechanisms can emulate multiple inheritance in PHP?");
        $question->setMultiple(true);
        $answer = new Answer();
        $answer->setText("Interfaces");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Namespaces");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Attributes & Method scopes(visibilities)");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Traits");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Exceptions");
        $question->addAnswer($answer);
        $em->persist($answer);
        $em->persist($question);

        $question = new Question();
        $question->setText("Which of these definitions stands for the MVC acronym?");
        $question->setMultiple(false);
        $answer = new Answer();
        $answer->setText("Model, View, Controller");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Visual Modelling of the Controller");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Virtual Model of Code");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Model, View, Client");
        $question->addAnswer($answer);
        $em->persist($answer);
        $em->persist($question);

        $question = new Question();
        $question->setText("Howe does the series of characters located after the question mark(?) in the following URL address?");
        $question->setMultiple(false);
        $answer = new Answer();
        $answer->setText("The protocol");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("The port number");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("The query string");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("The CGI script");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("The domain name");
        $question->addAnswer($answer);
        $em->persist($answer);
        $em->persist($question);

        $question = new Question();
        $question->setText('20. In the following example, which object is returned by the request() method call and stored in the $var variable?
<pre>
$var = $client->request(\'get\', \'/home\');
</pre>');
        $question->setMultiple(false);
        $answer = new Answer();
        $answer->setText("The Kernel");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("The client");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("The Profiler");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("The Service Container");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("The Crawler");
        $question->addAnswer($answer);
        $em->persist($answer);
        $em->persist($question);

        $question = new Question();
        $question->setText("Which of these assertions are true about Symfony2?");
        $question->setMultiple(true);
        $answer = new Answer();
        $answer->setText("Symfony is a set of autonomous, reusable and decoupled components.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Symfony is a relational database management system.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Symfony is a full-stack web framework.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Symfony is an ERP + ETL + CRM and PIM application");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Symfony is a content management system.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $em->persist($question);

        $question = new Question();
        $question->setText("What is the group of the status codes in the range 300-399?");
        $question->setMultiple(false);
        $answer = new Answer();
        $answer->setText("Redirects");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Information");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Client errors");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Server errors");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Success");
        $question->addAnswer($answer);
        $em->persist($answer);
        $em->persist($question);

        $question = new Question();
        $question->setText("Which one of the following headers can be attached to a HTTP response?");
        $question->setMultiple(true);
        $answer = new Answer();
        $answer->setText("Authorisation");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Referer");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Location");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Host");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Cache-Control");
        $question->addAnswer($answer);
        $em->persist($answer);
        $em->persist($question);

        $question = new Question();
        $question->setText("In a form, which constraints can be applied to a required field meant to receive an UUID value such as 7c23b323-b72c-4068-a5c5-6a97f71af4bd (before Symfony 2.5)?");
        $question->setMultiple(true);
        $answer = new Answer();
        $answer->setText("None of them");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Length");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Uuid");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Regex");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("NotBlank");
        $question->addAnswer($answer);
        $em->persist($answer);
        $em->persist($question);

        $question = new Question();
        $question->setText("What is the current stable version of Symfony supported for three years?");
        $question->setMultiple(false);
        $answer = new Answer();
        $answer->setText("Symfony 2.5");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Symfony 2.0");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Symfony 2.2");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Symfony 2.1");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Symfony 2.4");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Symfony 2.3");
        $question->addAnswer($answer);
        $em->persist($answer);
        $em->persist($question);

        $question = new Question();
        $question->setText("Which special method is meant to provide a default state to an object when its class is instantiated?");
        $question->setMultiple(false);
        $answer = new Answer();
        $answer->setText("The constructor");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("The creator");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("The configurator");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("The factory");
        $question->addAnswer($answer);
        $em->persist($answer);
        $em->persist($question);

        $question = new Question();
        $question->setText("What is the best location to execute custom SQL queries to select or persist data?");
        $question->setMultiple(false);
        $answer = new Answer();
        $answer->setText("It is not possible to execute custom SQL queries with Doctrine.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Inside a controller class.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Inside a Doctrine Repository class.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Inside a business/domain service.");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Inside a Twig template, thanks to the doctrine_query() function");
        $question->addAnswer($answer);
        $em->persist($answer);
        $em->persist($question);

        $question = new Question();
        $question->setText("Which of the following assertions correspond to a control flow structure?");
        $question->setMultiple(true);
        $answer = new Answer();
        $answer->setText("Loops");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Methods");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Arrays");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Conditions");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Variables");
        $question->addAnswer($answer);
        $em->persist($answer);
        $em->persist($question);

        $question = new Question();
        $question->setText("Which one of the following attacks allows a hacker to run malicious JavaScript code in an HTML page?");
        $question->setMultiple(false);
        $answer = new Answer();
        $answer->setText("Cross Site Request Forgery");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Sensitive Data Exposure");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("IP Address Spoofing");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Session Hijacking");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Cross Site Scripting");
        $question->addAnswer($answer);
        $em->persist($answer);
        $em->persist($question);

        $question = new Question();
        $question->setText("30. Which of these libraries is not natively embedded in Symfony2?");
        $question->setMultiple(false);
        $answer = new Answer();
        $answer->setText("Twig");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Monolog");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Doctrine");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Assetic");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Propel");
        $question->addAnswer($answer);
        $em->persist($answer);
        $em->persist($question);

        $question = new Question();
        $question->setText("Which of these visibilities allows to control the access to properties and methods only inside a class and its subtypes?");
        $question->setMultiple(false);
        $answer = new Answer();
        $answer->setText("Protected");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Private");
        $question->addAnswer($answer);
        $em->persist($answer);
        $answer = new Answer();
        $answer->setText("Public");
        $question->addAnswer($answer);
        $em->persist($answer);
        $em->persist($question);

        $em->flush();
    }
}
