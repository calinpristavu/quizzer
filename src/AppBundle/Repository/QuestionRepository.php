<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class QuestionRepository
 *
 * @package AppBundle\Repository
 * @author  Calin Pristavu <calin.pristavu@evozon.com>
 */
class QuestionRepository extends EntityRepository
{

}
