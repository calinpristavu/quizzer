<?php
namespace AppBundle\Form\Type;

use AppBundle\Entity\Question;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class QuestionType
 *
 * @package AppBundle\Form\Type
 * @author  Calin Pristavu <calin.pristavu@evozon.com>
 */
class QuestionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Question $question */
        $question = $options['data'];

        $builder
            ->add('answers', 'entity', [
                'class' => 'AppBundle\Entity\Answer',
                'mapped' => false,
                'multiple' => $question->isMultiple(),
                'expanded' => true,
                'query_builder' => function (EntityRepository $repo) use ($question) {
                    return $repo
                        ->createQueryBuilder('a')
                        ->where('a.question = ?1')
                        ->setParameters([
                            1 => $question
                        ]);
                }
            ]);
    }

    /**
     * Configures the options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options.
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'multiple_answers' => false,
            'mapped' => false
        ]);
    }
}
