<?php
declare(strict_types = 1);
namespace AppBundle\Service;

use AppBundle\Entity\Answer;
use AppBundle\Entity\Question;
use AppBundle\Entity\Quiz;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;


class QuizHandler
{

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var QuizGenerator
     */
    protected $quizGenerator;

    function __construct(EntityManagerInterface $entityManager, QuizGenerator $quizGenerator)
    {
        $this->entityManager = $entityManager;
        $this->quizGenerator = $quizGenerator;
    }


    public function handleQuiz($answers, Question $question, Quiz $quiz)
    {
        $correctQuestion = true;
        /** @var ArrayCollection $answers */
        if (!$answers instanceof ArrayCollection) {
            $answers = new ArrayCollection([$answers]);
        }
        if ($answers->count() !== $question->getCorrectAnswers()->count()) {
            $correctQuestion = false;
        }
        /** @var Answer $answer */
        foreach ($answers as $answer) {
            /** @var Quiz $quiz */
            $quiz->addAnswer($answer);
            if (!$answer->isCorrect() && $correctQuestion) {
                $correctQuestion = false;
            }
        }
        if ($correctQuestion) {
            $quiz->incrementCorrect();
        }
        $quiz->incrementAnswered();
        $quiz->setActiveQuestion($this->quizGenerator->getQuestion($quiz));

        $this->entityManager->flush();
    }
}
