<?php
declare(strict_types = 1);
namespace AppBundle\Service;

use AppBundle\Entity\Question;
use AppBundle\Entity\Quiz;
use AppBundle\Repository\QuestionRepository;

/**
 * Class QuizGenerator
 *
 * @package AppBundle\Service
 * @author  Calin Pristavu <calin.pristavu@evozon.com>
 */
class QuizGenerator
{

    /**
     * @var QuestionRepository
     */
    private $questionRepo;

    /**
     * QuizGenerator constructor.
     *
     * @param QuestionRepository $questionRepo
     */
    public function __construct(QuestionRepository $questionRepo)
    {
        $this->questionRepo = $questionRepo;
    }

    /**
     * @param Quiz $quiz
     *
     * @return Question
     */
    public function getQuestion(Quiz $quiz) : Question
    {
        $questions = $this->questionRepo->findAll();
        /** @var Question $selectedQuestion */
        do {
            $selectedQuestionKey = array_rand($questions);

            $selectedQuestion = $questions[$selectedQuestionKey];
        } while ($quiz->getAnswers()->contains($selectedQuestion));

        return $selectedQuestion;
    }
}
