<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Quiz;
use AppBundle\Entity\User;
use AppBundle\Form\Type\QuestionType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class QuizController
 *
 * @package AppBundle\Controller
 * @author  Calin Pristavu <calin.pristavu@evozon.com>
 */
class QuizController extends Controller
{
    /**
     * @Route(name="quiz_start", path="/start")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function startQuizAction()
    {
        $quiz = new Quiz();
        $quiz->setActiveQuestion($this->get('app.quiz_generator')->getQuestion($quiz));
        /** @var User $user */
        $user = $this->getUser();
        if ($user) {
            $user->addQuiz($quiz);
        } else {
            // crapa aplicatia?
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($quiz);
        $em->flush();

        return $this->redirectToRoute('quiz_question', [
            'id' => $quiz->getId()
        ]);
    }

    /**
     * @Route(name="quiz_question", path="/question/{id}")
     * @ParamConverter("quiz", class="AppBundle:Quiz")
     * @param Request  $request
     * @param Quiz     $quiz
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function questionAction(Request $request, Quiz $quiz): Response
    {
        if ($quiz->getNoOfQuestions() === $quiz->getAnsweredQuestions()) {
            return $this->redirectToRoute('quiz_result', [
                'id' => $quiz->getId()
            ]);
        }
        $question = $quiz->getActiveQuestion();
        $questionForm = $this->createForm(QuestionType::class, $question);
        $questionForm->handleRequest($request);
        if ($questionForm->isValid()) {
            $this->get('app.quiz_handler')->handleQuiz($questionForm->get('answers')->getData(), $question, $quiz);

            return $this->redirectToRoute('quiz_question', [
                'id' => $quiz->getId()
            ]);
        }

        return $this->render('AppBundle:Quiz:question.html.twig', [
            'question' => $question,
            'quiz' => $quiz,
            'form' => $questionForm->createView()
        ]);
    }

    /**
     * @Route(name="quiz_result", path="/result/{id}")
     * @ParamConverter("quiz", class="AppBundle:Quiz")
     * @param Quiz $quiz
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function resultAction(Quiz $quiz): Response
    {
        $percentCorrect = $quiz->getNoCorrectAnswers() * 100 / $quiz->getNoOfQuestions();
        $passed = false;
        if ($percentCorrect >= Quiz::PASS_PERCENT) {
            $passed = true;
        }

        return $this->render('AppBundle:Quiz:result.html.twig', [
            'quiz' => $quiz,
            'passed' => $passed
        ]);
    }
}
