<?php

namespace NS\ScratchBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * Class ScratchControllerExtension
 *
 * @package site\src\NS\ScratchBundle\DependencyInjection
 * @author  Calin Pristavu <calin.pristavu@evozon.com>
 */
class ScratchControllerExtension extends Extension
{

    /**
     * Loads a specific configuration.
     *
     * @param array            $config    An array of configuration values
     * @param ContainerBuilder $container A ContainerBuilder instance
     *
     * @throws \InvalidArgumentException When provided tag is not defined in this extension
     */
    public function load(array $config, ContainerBuilder $container)
    {
        // load any configuration defined in app/config/config.yml
        $globalConfig = new Configuration();
        $this->processConfiguration($globalConfig, $config);

        // load services from services.yml
        $configLoader = new YamlFileLoader($container, new FileLocator(__DIR__ . "/../Resources/config"));
        $configLoader->load("services.yml");
    }
}
