<?php

namespace NS\ScratchBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class ScratchControllerBundle
 *
 * @package NS\ScratchBundle
 * @author  Calin Pristavu <calin.pristavu@evozon.com>
 */
class NSScratchBundle extends Bundle
{

}
