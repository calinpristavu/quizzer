<?php

namespace NS\ScratchBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class ScratchController
 *
 * @package site\src\NS\ScratchBundle\Controller
 * @author  Calin Pristavu <calin.pristavu@evozon.com>
 */
class ScratchController extends Controller
{

}
